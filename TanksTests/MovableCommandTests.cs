using System;
using Xunit;
using core;
using Moq;
using tank_interface;
using System.Threading;
using System.Threading.Tasks;

namespace Tank.Test
{
    public class MovableCommandTests
    {
        [Fact]
        public void MoveCommandTest()
        {
            Moveable moveable = new Moveable();
            moveable.Position = new Vector(new int[] { 2, 1 });
            moveable.Velocity = new Vector(new int[] { 3, 5 });

            MoveCommand moveCommand = new MoveCommand(moveable);
            Assert.True(Vector.Equals(moveable.Position, new Vector(2, 1)));
            moveCommand.Execute();
            Assert.True(Vector.Equals(moveable.Position, new Vector(5, 6)));

            moveCommand.Execute();
            Assert.True(Vector.Equals(moveable.Position, new Vector(8, 11)));
        }

        [Fact]
        public void MoveCommandShouldMoveObject()
        {
            //Arrange
            var m = new Mock<IMoveable>();

            m.SetupGet(m => m.Position).Returns(new Vector(new int[] { 2, 1 })).Verifiable();
            m.SetupGet(m => m.Velocity).Returns(new Vector(new int[] { 3, 5 })).Verifiable();
            m.SetupSet(m => m.Position = It.IsAny<Vector>()).Verifiable();

            MoveCommand c = new MoveCommand(m.Object);

            //Act
            c.Execute();

            //Assert
            m.VerifyAll();
        }

        [Fact]
        public void ContinueMovableTest5Seconds()
        {
            Moveable moveable = new Moveable();
            moveable.Position = new Vector(new int[] { 2, 1 });
            moveable.Velocity = new Vector(new int[] { 3, 5 });
            moveable.Injector = new Injector(null);

            ContinueMove continueMove = new ContinueMove(moveable);

            Task.Factory.StartNew(() =>
            {
                continueMove.Execute();
            });

            StopMovable stopMovable = new StopMovable((Injector)moveable.Injector);
            MoveStop moveStop = new MoveStop(stopMovable);

            Thread.Sleep(5000);

            moveStop.Execute();
            Thread.Sleep(1500);

            Assert.True(Vector.Equals(moveable.Position, new Vector(17, 26)));
        }

        [Fact]
        public void ContinueMovableTest3Second()
        {
            Moveable moveable = new Moveable();
            moveable.Position = new Vector(new int[] { 2, 1 });
            moveable.Velocity = new Vector(new int[] { 3, 5 });
            moveable.Injector = new Injector(null);

            ContinueMove continueMove = new ContinueMove(moveable);

            Task.Factory.StartNew(() =>
            {
                continueMove.Execute();
            });

            StopMovable stopMovable = new StopMovable((Injector)moveable.Injector);
            MoveStop moveStop = new MoveStop(stopMovable);

            Thread.Sleep(3000);

            moveStop.Execute();
            Thread.Sleep(1500);

            Assert.True(Vector.Equals(moveable.Position, new Vector(11, 16)));
        }

        [Fact]
        public void TestSetupGetMove1()
        {
            //Arrange
            var m = new Mock<IMoveable>();

            m.SetupGet(m => m.Position).Throws(new Exception()).Verifiable();
            m.SetupGet(m => m.Velocity).Returns(new Vector(new int[] { 3, 5 })).Verifiable();
            m.SetupSet(
                m => m.Position = It.Is<Vector>(
                    v => v.Equals(new Vector(new int[] { 5, 6 }))
                )
            ).Verifiable();

            MoveCommand c = new MoveCommand(m.Object);

            Assert.Throws<Exception>(() => c.Execute());
        }

        [Fact]
        public void TestSetupSetMove2()
        {
            //Arrange
            var m = new Mock<IMoveable>();

            m.SetupGet(m => m.Position).Returns(new Vector(new int[] { 2, 1 }));
            m.SetupGet(m => m.Velocity).Returns(new Vector(new int[] { 3, 5 }));
            m.SetupSet(m => m.Position = It.IsAny<Vector>()).Throws(new Exception());

            MoveCommand c = new MoveCommand(m.Object);

            //Act
            Assert.Throws<Exception>(() => c.Execute());
        }

        [Fact]
        public void TestSetupSetMove3()
        {
            //arrange
            var m = new Mock<IMoveable>();

            m.SetupGet(m => m.Velocity).Returns(new Vector(2, 1)).Verifiable();
            m.SetupGet(m => m.Position).Throws(new Exception()).Verifiable();
            m.SetupSet(m => m.Position = It.IsAny<Vector>()).Verifiable();
            //action
            MoveCommand c = new MoveCommand(m.Object);
            Assert.Throws<Exception>(() => c.Execute());
        }

        [Fact]
        public void TestSetupSetMove4()
        {
            //arrange
            var m = new Mock<IMoveable>();

            m.SetupGet(m => m.Position).Throws(new Exception()).Verifiable();
            m.SetupGet(m => m.Velocity).Returns(new Vector(new int[] { 3, 5 }));
            m.SetupSet(m => m.Position = It.IsAny<Vector>()).Verifiable();
            //action
            MoveCommand c = new MoveCommand(m.Object);
            Assert.Throws<Exception>(() => c.Execute());
        }
    }
}