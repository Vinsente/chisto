using System;
using Xunit;
using core;
using Moq;
using tank_interface;

namespace Tank.Test
{
    public class VectorTests
    {

        [Fact]
        public void TestVectorEqualsTrue()
        {
            var v1 = new Vector(1, 2, 3);
            var v2 = new Vector(1, 2, 3);
            Assert.True(Vector.Equals(v1, v2));
        }
        [Fact]
        public void TestVectorEqualsFalse()
        {
            var v1 = new Vector(1, 2, 3);
            var v2 = new Vector(1, 2);
            Assert.False(Vector.Equals(v1, v2));
        }
        [Fact]
        public void TestVectorSumIsCorrect()
        {
            var v1 = new Vector(1, 10);
            var v2 = new Vector(1, 2);
            var v3 = new Vector(2, 12);
            Assert.True(Vector.Equals(v3, v1 + v2));
        }
        [Fact]
        public void TestVectorSumIsNotCorrect()
        {
            var v1 = new Vector(1, -2);
            var v2 = new Vector(5, 3);
            var v3 = new Vector(2, 0);
            Assert.False(Vector.Equals(v3, v1 + v2));
        }
    }
}