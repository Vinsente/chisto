﻿using tank_interface;

namespace Tanks
{
    internal class Queue : IQueue
    {
        private List<ICommand> commands;

        public Queue()
        {
            commands = new List<ICommand>();
        }

        public void Add(ICommand cmd)
        {
            commands.Add(cmd);
        }
    }
}
