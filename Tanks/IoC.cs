using tank_interface;

namespace core
{
    public class IoC
    {
        public static T Resolve<T>(string key, params object[] args)
        {
            return (T)model(key, args);
        }
        
        public class IoCSetupCommand : ICommand
        {
            Func<string, object[], object> newModel;
            public IoCSetupCommand(Func<string, object[], object> newModel)
            {
                this.newModel = newModel;
            }
            public void Execute()
            {
                model = newModel;
            }
        }
        
        public static Func<string, object[], object> model = (key, args) =>
        {
            if ("IoC.Setup" == key)
            {
                var newModel = (Func<string, object[], object>)args[0];
                return new IoCSetupCommand(newModel);
            }
            else if ("IoC.Model" == key)
            {
                return model;
            }
            else if ("PushCommand" == key)
            {
                return (Injector)args[0];
            }
            else if ("EmptyCommand" == key)
            {
                return new EmptyCommand();
            }
            else
            {
                throw new Exception("Unknown key");
            }
        };

     
    }

}
