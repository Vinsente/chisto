﻿using tank_interface;

namespace core
{
    public class EmptyCommand : ICommand
    {
        public void Execute()
        {
        }
    }

    public class Moveable : IMoveable
    {
        public Vector Position { get; set; }

        public Vector Velocity { get; set; }

        public ICommand Injector { get; set; }
    }

    public class MoveCommand : ICommand
    {
        public IMoveable m;
        public MoveCommand(IMoveable movable)
        {
            this.m = movable;
        }

        public void Execute()
        {
            m.Position += m.Velocity;
        }
    }

    public class StartMoveCommand : ICommand
    {
        IMoveStartable moveStartable;
        public StartMoveCommand(IMoveStartable moveStartable)
        {
            this.moveStartable = moveStartable;
        }

        public void Execute()
        {
            ICommand MoveCMD = IoC.Resolve<ICommand>("ChangeVelocity", moveStartable);
            ICommand Injector = IoC.Resolve<ICommand>("Injector", MoveCMD);
            moveStartable.SetInjector = Injector;
            ICommand cmd = IoC.Resolve<ICommand>("PushCommand", Injector);
            cmd.Execute();
        }
    }

    public class MoveStop : ICommand
    {
        IStopMovable m;

        public MoveStop(IStopMovable movable)
        {
            this.m = movable;
        }

        public void Execute()
        {
            Injector MoveCommand = m.MoveStopCMD();
            ICommand EmptyCommand = IoC.Resolve<ICommand>("EmptyCommand");

            MoveCommand.Command = EmptyCommand;

            m.RemoveVeloctiy();
        }
    }

    public class ContinueMove : ICommand
    {
        IMoveable m;
        public ContinueMove(IMoveable movable)
        {
            m = movable;
        }

        public void Execute()
        {
            m.Position += m.Velocity;
            Injector injector = (Injector)m.Injector;
            injector.Command = this;
            Injector cmd = IoC.Resolve<Injector>("PushCommand", injector);

            Thread.Sleep(1000);
            if (cmd.Command is EmptyCommand)
            {
                return;
            }
            else
            {
                cmd.Execute();
            }
        }
    }

    public class StopMovable : IStopMovable
    {
        private Injector injector;

        public StopMovable(Injector injector)
        {
            this.injector = injector;
        }

        public Injector MoveStopCMD()
        {
            return injector;
        }

        public void RemoveVeloctiy()
        {
        }
    }
}
