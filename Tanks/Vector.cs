public class Vector
{
    int[] val;
    public int Size
    {
        get => val.Length;
    }
    public Vector(params int[] val)
    {
        this.val = val;
    }
    public static bool Equals(Vector v1, Vector v2)
    {
        return Enumerable.SequenceEqual(v1.val, v2.val);
    }

    public static bool IsSameSize(Vector v1, Vector v2)
    {
        return v1.val.Length == v2.val.Length;
    }

    public static Vector operator +(Vector v1, Vector v2)
    {
        int[] v = new int[v1.Size];
        for (int i = 0; i < v1.Size; i++)
        {
            v[i] = v1.val[i] + v2.val[i];
        }
        return new Vector(v);
    }

    public int this[int index]
    {
        get
        {
            return val[index];
        }
    }
}