using tank_interface;

namespace core
{
    public class Injector : ICommand
    {
        public Injector(ICommand command)
        {
            Command = command;
        }


        public ICommand Command
        {
            get; set;
        }

        public void Execute()
        {
            Command.Execute();
        }
    }
}
