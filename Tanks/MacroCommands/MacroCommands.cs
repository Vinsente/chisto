using tank_interface;

namespace core 
{
public class MacroCommand : ICommand
{
    List<ICommand> cmd_list;
    public MacroCommand(List<ICommand> cmd_list)
    {
        this.cmd_list = cmd_list;
    }
    public MacroCommand() {
        cmd_list = new List<ICommand>()
        { 

        };
    }
    public void Execute()
    {
        foreach(var c in cmd_list)
        {
            c.Execute();
        }
    }
  }
}
