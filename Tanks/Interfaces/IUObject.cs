namespace tank_interface 
{
 public interface IUObject
    {
        object this[string key]
        {
            get;
            set;
        }
    }
}