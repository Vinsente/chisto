using core;

namespace tank_interface
{
    public interface IMoveable
    {
        Vector Position { get; set; }
        Vector Velocity { get; }
        ICommand Injector
        {
            get;
        }
    }

    public interface IMoveStartable
    {
        string ID
        {
            get;
        }

        ICommand SetInjector
        {
            get;
            set;
        }
    }

    public interface IStopMovable
    {
        Injector MoveStopCMD();
        void RemoveVeloctiy();
    }
}
