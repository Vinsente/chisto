namespace tank_interface {
    public interface IQueue {
        void Add(ICommand cmd);
    }
}
