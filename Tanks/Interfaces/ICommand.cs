namespace tank_interface
{
    public interface ICommand
    {
        void Execute();
    }
}
